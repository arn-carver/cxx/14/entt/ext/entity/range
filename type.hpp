#pragma once

#include <utility>

namespace entt {
    namespace ext {
        namespace entity {
            namespace range {
                template<typename Entity>
                using type = std::pair<Entity, Entity>;

                template<typename Entity> inline
                type<Entity> make(Entity first, Entity last) {
                    return {first, last}
                }
            }
        }
    }
}