#pragma once

#include "tag.hpp"
#include "sparse_set.hpp"

#include <entt/entity/view.hpp>

namespace entt {

    template<typename Entity, typename Component>
    class ViewExtension<ext::entity::range::tag, Entity, Component> {
    public:
        using ext_tag = ext::entity::range::tag;
        using extendable_type = View<Entity, Component>;
    private:
        using this_type = ViewExtension<ext::entity::range::tag, Entity, Component>;
        using internal_extension_type = extendable_type::view_type::extension_type<ext_tag>;
    public:
        using entity_type = Entity;
        /*! @brief Underlying entity range type. */
        using entity_range_type = typename internal_extension_type::entity_range_type;
        /*! @brief Input range type. */
        using range_type = typename internal_extension_type::range_type;

        using const_range_type = typename internal_extension_type::const_range_type;

        ViewExtension(extendable_type* view)  ENTT_NOEXCEPT : 
            internal_extension{ view->pool.extension<ext_tag>() },
            extendable{ view }
        {}
        
        extendable_type* operator ->() ENTT_NOEXCEPT {
            return extendable;
        }

        range_type range(const entity_range_type& entity_range) ENTT_NOEXCEPT {
            return internal_extension.range(entity_range);
        }

        const_range_type range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            return internal_extension.range(entity_range);
        }

        template<typename Func>
        void within(const entity_range_type& entity_range, Func func) const {
            for (auto entity : range(entity_range)) {
                func(entity, extendable->get(entity));
            }
        }
        template<typename Func>
        void within(const entity_range_type& entity_range, Func func) {
            const_cast<const this_type *>(this)->within(entity_range, [&func](entity_type entity, const Component &component) {
                func(entity, const_cast<Component &>(component));
            });
        }
    private:
        internal_extension_type internal_extension;
        extendable_type* extendable = nullptr;
    };

    template<typename Entity, typename... Component>
    class ViewExtension<ext::entity::range::tag, Entity, Component...> {
    public:
        using ext_tag = ext::entity::range::tag;
        using extendable_type = View<Entity, Component...>;
    };

    template<typename Entity, typename Component>
    class RawViewExtension<ext::entity::range::tag, Entity, Component> {
    public:
        using ext_tag = ext::entity::range::tag;
        using extendable_type = RawView<Entity, Component>;
    private:
        using pool_extension_type = extendable_type::pool_type::extension_type<ext_tag>;
    public:
        /*! @brief Underlying entity range type. */
        using entity_range_type = typename pool_extension_type::entity_range_type;
        /*! @brief Input range type. */
        using range_type = typename pool_extension_type::range_type;

        using const_range_type = typename pool_extension_type::const_range_type;

        RawViewExtension(extendable_type* view) ENTT_NOEXCEPT :
            pool_extension{ view->pool.extension<ext_tag>() },
            extendable{ view }
        {}

        extendable_type* operator ->() ENTT_NOEXCEPT {
            return extendable;
        }

        range_type range(const entity_range_type& entity_range) ENTT_NOEXCEPT {
            return pool_extension.range(entity_range);
        }
        const_range_type range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            return pool_extension.range(entity_range);
        }
    private:
        pool_extension_type pool_extension;
        extendable_type* extendable = nullptr;
    };

    template<typename Entity, typename... Component>
    class PersistentViewExtension<ext::entity::range::tag, Entity, Component...> {
    public:
        using ext_tag = ext::entity::range::tag;
        using extendable_type = PersistentView<Entity, Component...>;
    private:
        using this_type = PersistentViewExtension<ext::entity::range::tag, Entity, Component...>;
        using internal_extension_type = extendable_type::view_type::extension_type<ext_tag>;
    public:
        using entity_type = Entity;
        /*! @brief Underlying entity range type. */
        using entity_range_type = typename internal_extension_type::entity_range_type;
        /*! @brief Input range type. */
        using range_type = typename internal_extension_type::range_type;

        using const_range_type = typename internal_extension_type::const_range_type;

        PersistentViewExtension(extendable_type* view)  ENTT_NOEXCEPT :
            internal_extension{ view->view.extension<ext_tag>() },
            extendable{ view }
        {}

        extendable_type* operator ->() ENTT_NOEXCEPT {
            return extendable;
        }

        range_type range(const entity_range_type& entity_range) ENTT_NOEXCEPT {
            return internal_extension.range(entity_range);
        }

        const_range_type range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            return internal_extension.range(entity_range);
        }

        template<typename Func>
        void within(const entity_range_type& entity_range, Func func) const {
            for (auto entity : range(entity_range)) {
                func(entity, extendable->get<Component>(entity)...);
            }
        }
        template<typename Func>
        void within(const entity_range_type& entity_range, Func func) {
            const_cast<const this_type *>(this)->within(entity_range, [&func](entity_type entity, const Component &... component) {
                func(entity, const_cast<Component &>(component)...);
            });
        }
    private:
        internal_extension_type internal_extension;
        extendable_type* extendable = nullptr;
    };

}
