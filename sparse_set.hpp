#pragma once

#include "tag.hpp"
#include "type.hpp"

#include <entt/entity/sparse_set.hpp>

namespace entt {
    
    template <typename Entity> 
    class SparseSetExtension<ext::entity::range::tag, Entity>
    {
    public:
        using ext_tag = ext::entity::range::tag;
        using traits_type = entt_traits<Entity>;
        using extendable_type = SparseSet<Entity>;
        using iterator_type = typename extendable_type::iterator_type;
    private:
        struct IteratorRange final {
            using pointer = typename iterator_type::pointer;

            IteratorRange(pointer direct) :
                rbegin{ direct, 0 },
                rend{ direct, 1 }
            { }
            IteratorRange(pointer direct, const std::pair<std::size_t, std::size_t>& pos_range) :
                rbegin{ direct, pos_range.second },
                rend{ direct, pos_range.first }
            { }

            bool valid() const ENTT_NOEXCEPT {
                return rbegin.pos >= rend.pos;
            }

            void invalidate() ENTT_NOEXCEPT {
                rbegin.pos = 0;
                rend.pos = 1;
            }

            iterator_type begin() const ENTT_NOEXCEPT {
                return rbegin;
            }
            iterator_type end() const ENTT_NOEXCEPT {
                return rend;
            }
        private:
            iterator_type rbegin;
            iterator_type rend;
        };
    public:
        /*! @brief Underlying entity identifier. */
        using entity_type = Entity;
        /*! @brief Underlying entity range type. */
        using entity_range_type = ext::entity::range::type<entity_type>;
        /*! @brief Entity dependent position type. */
        using pos_type = entity_type;
        /*! @brief Entity dependent position range type. */
        using pos_range_type = std::pair<pos_type, pos_type>;
        /*! @brief Input range type. */
        using range_type = IteratorRange;

        using const_range_type = IteratorRange;

        using size_type = typename extendable_type::size_type;

        SparseSetExtension() ENTT_NOEXCEPT = default;
        SparseSetExtension(extendable_type* sparse_set) :
            extendable{ sparse_set }
        { }

        /*! @brief Default destructor. */
        virtual ~SparseSetExtension() ENTT_NOEXCEPT = default;

        /*! @brief Copying a sparse set range isn't allowed. */
        SparseSetExtension(const SparseSetExtension &) = delete;
        /*! @brief Default move constructor. */
        SparseSetExtension(SparseSetExtension &&) = default;

        /*! @brief Copying a sparse set range isn't allowed. @return This sparse set range. */
        SparseSetExtension & operator=(const SparseSetExtension &) = delete;
        /*! @brief Default move assignment operator. @return This sparse set range. */
        SparseSetExtension & operator=(SparseSetExtension &&) = default;

        SparseSetExtension & operator=(extendable_type* extendable) {
            this->extendable = extendable;
            return *this;
        }
        /**
        * @brief Returns an entity range.
        *
        * The returned range of iterators that points to the elements within entity range in
        * the internal packed array.
        *
        * @note
        * Range stay true to the order imposed by a call to `respect`.
        *
        * @return An range of iterators to the elements within entity range in
        * the internal packed array.
        */
        range_type range(const entity_range_type& entity_range) ENTT_NOEXCEPT {
            return range_type{ extendable->direct.data(), make_pos_range(entity_range) };
        }

        const_range_type range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            return const_range_type{ extendable->direct.data(), make_pos_range(entity_range) };
        }

        entity_range_type make_entity_range(entity_type first, entity_type last) const ENTT_NOEXCEPT {
            return {first, last };
        }

        bool contains(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            const auto pos_range = make_pos_range(entity_range);
            return pos_range.first < pos_range.second;
        }
        /**
        * @brief Removes an range of entities from a sparse set.
        *
        * @warning
        * Attempting to remove an entity that doesn't belong to the sparse set
        * results nothing.<br/>
        *
        * @param entity_range A valid entity identifier range.
        */
        virtual bool destroy(const entity_range_type& entity_range) {
            auto pos_range = make_pos_range(entity_range);
            auto found = pos_range.first < pos_range.second;

            if (found) {
                pos_type erase;
                pos_type back;
                pos_type next;
                pos_type not_used;

                prepare_range_destroy(pos_range, erase, back, next, not_used);
                direct_range_destroy(pos_range, erase, back, next, not_used);
            }

            return found;
        }

        extendable_type* operator ->() ENTT_NOEXCEPT {
            return extendable;
        }
    protected:

        pos_range_type make_pos_range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            pos_range_type out;
            pos_type pos;

            if (extendable->has(entity_range.first)) {
                out.first = extendable->get(entity_range.first);

                if (entity_range.first == entity_range.second) {
                    out.second = out.first + 1;
                }
                else if (extendable->has(entity_range.second)) {
                    pos = extendable->get(entity_range.second);

                    out.second = pos;

                    if (out.first > out.second) {
                        std::swap(out.first, out.second);
                    }

                    ++out.second;
                }
                else {
                    out.second = out.first;
                }

                if (out.first < out.second) {
                    return out;
                }
            }
            else if (entity_range.first == entity_range.second) {
                out.first = 0;
                out.second = 0;

                return out;
            }

            out.first = 1;
            out.second = 0;

            return out;
        }

        entity_range_type make_entity_range(const pos_range_type& pos_range) const ENTT_NOEXCEPT {
            return { extendable->direct[pos_range.first], extendable->direct[pos_range.second - 1] };
        }

        void prepare_range_destroy(
            const pos_range_type& pos_range,
            pos_type& out_erase,
            pos_type& out_back,
            pos_type& out_next,
            pos_type& out_not_used) const ENTT_NOEXCEPT
        {
            const auto size = extendable->direct.size();
            const auto length = pos_range.second - pos_range.first;

            out_erase = size - length;

            out_back = size;
            out_next = pos_range.second + length;

            if (out_next > size) {
                auto distance = size - pos_range.second;

                out_back -= distance;
                out_not_used = pos_range.first + distance;
            }
            else {
                out_back -= length;
                out_not_used = size;
            }
        }


        void direct_range_destroy(
            const pos_range_type& pos_range,
            const pos_type& erase,
            const pos_type& back,
            const pos_type& next,
            const pos_type& not_used) ENTT_NOEXCEPT
        {
            auto& reverse = extendable->reverse;
            auto& direct = extendable->direct;

            const auto size = direct.size();

            auto* entity_ptr = &direct[pos_range.first];

            for (auto i = not_used; i < pos_range.second; ++i) {
                auto entt = direct[i] & traits_type::entity_mask;

                reverse[entt] &= traits_type::entity_mask;
            }

            for (auto i = size - 1; back <= i; --i, ++entity_ptr) {
                auto& entity_ref = *entity_ptr;
                auto& back_ref = direct[i];

                const auto entity_pos = reverse[entity_ref & traits_type::entity_mask] & traits_type::entity_mask;
                const auto back_pos = reverse[back_ref & traits_type::entity_mask] & traits_type::entity_mask;

                reverse[back_pos] = reverse[entity_pos];
                reverse[entity_pos] &= traits_type::entity_mask;

                entity_ref = back_ref;
            }

            direct.erase(direct.begin() + erase, direct.end());
        }
    protected:
        extendable_type* extendable = nullptr;
    };

    template<typename Entity, typename Type>
    class   SparseSetExtension<ext::entity::range::tag, Entity, Type> :
    public  SparseSetExtension<ext::entity::range::tag, Entity>
    {
    public:
        using ext_tag = ext::entity::range::tag;
        using extendable_type = SparseSet<Entity, Type>;

        template<bool Const>
        using iterator_type = typename extendable_type::Iterator<Const>;
    private:
        using underlying_type = SparseSetExtension<ext_tag, Entity>;

        template<bool Const>
        struct IteratorRange final {
            using underlying_iterator_type = iterator_type<Const>;
            using pointer = typename underlying_iterator_type::pointer;

            IteratorRange(pointer instances) :
                rbegin{ instances, 0 },
                rend{ instances, 1 }
            { }
            IteratorRange(pointer instances, const pos_range_type& pos_range) :
                rbegin{ instances, pos_range.second },
                rend{ instances, pos_range.first }
            { }

            bool valid() const ENTT_NOEXCEPT {
                return rbegin.pos >= rend.pos;
            }

            void invalidate() ENTT_NOEXCEPT {
                rbegin.pos = 0;
                rend.pos = 1;
            }

            underlying_iterator_type begin() const ENTT_NOEXCEPT {
                return rbegin;
            }
            underlying_iterator_type end() const ENTT_NOEXCEPT {
                return rend;
            }

            underlying_iterator_type rbegin;
            underlying_iterator_type rend;
        };
    public:
        /*! @brief Type of the objects associated to the entities. */
        using object_type = typename extendable_type::object_type;
        /*! @brief Input range type. */
        using range_type = IteratorRange<false>;
        using const_range_type = IteratorRange<true>;

        SparseSetExtension() ENTT_NOEXCEPT = default;
        SparseSetExtension(extendable_type* extendable_ptr) :
            extendable(extendable_ptr)
        {
            underlying_type::extendable = extendable_ptr;
        }

        /*! @brief Default destructor. */
        virtual ~SparseSetExtension() ENTT_NOEXCEPT = default;

        /*! @brief Copying a sparse set range isn't allowed. */
        SparseSetExtension(const SparseSetExtension &) = delete;
        /*! @brief Default move constructor. */
        SparseSetExtension(SparseSetExtension &&) = default;

        /*! @brief Copying a sparse set range isn't allowed. @return This sparse set range. */
        SparseSetExtension & operator=(const SparseSetExtension &) = delete;
        /*! @brief Default move assignment operator. @return This sparse set range. */
        SparseSetExtension & operator=(SparseSetExtension &&) = default;

        SparseSetExtension & operator=(extendable_type* extendable) {
            this->extendable = extendable;
            return *this;
        }
        /**
        * @brief Returns an entity range.
        *
        * The returned range of iterators that points to the elements following the instances
        * of the given type with in entity range.
        *
        * @note
        * Input range stay true to the order imposed by a call to either `sort`
        * or `respect`.
        *
        * @return An range of iterators to the elements following the instances
        * of the given type with in entity range.
        */
        range_type range(const entity_range_type& entity_range) ENTT_NOEXCEPT {
            return range_type{ extendable->instances.data(), underlying_type::make_pos_range(entity_range) };
        }

        const_range_type range(const entity_range_type& entity_range) const ENTT_NOEXCEPT {
            return const_range_type{ extendable->instances.data(), underlying_type::make_pos_range(entity_range) };
        }
        /**
        * @brief Removes an range of entities from a sparse set and destroies its object.
        *
        * @warning
        * Attempting to use an entity that doesn't belong to the sparse set results
        * nothing.<br/>
        *
        * @param entity_range A valid entity identifier range.
        */
        bool destroy(const entity_range_type& entity_range) override {
            auto pos_range = make_pos_range(entity_range);
            auto found = pos_range.first < pos_range.second;

            if (found) {
                pos_type erase;
                pos_type back;
                pos_type next;
                pos_type distance;

                prepare_range_destroy(pos_range, erase, back, next, distance);
                instance_range_destroy(pos_range, erase, back, next, distance);
                direct_range_destroy(pos_range, erase, back, next, distance);
            }

            return found;
        }

        template<typename Compare>
        void sort(entity_range_type& entity_range, Compare compare) {

            const auto pos_range = underlying_type::make_pos_range(entity_range);

            if (pos_range.first < pos_range.second) {

                std::vector<pos_type> copy(pos_range.second - pos_range.first);
                std::iota(copy.begin(), copy.end(), 0);

                auto* data = &extendable->instances[pos_range.first];

                std::sort(copy.begin(), copy.end(), [this, compare = std::move(compare), &data](auto lhs, auto rhs) {
                    return compare(const_cast<const object_type &>(data[rhs]), const_cast<const object_type &>(data[lhs]));
                });

                for (pos_type pos = 0, last = copy.size(); pos < last; ++pos) {
                    auto curr = pos;
                    auto next = copy[curr];

                    while (curr != next) {
                        const auto lhs = copy[curr];
                        const auto rhs = copy[next];
                        std::swap(data[lhs], data[rhs]);
                        underlying_type::extendable->swap(lhs + pos_range.first, rhs + pos_range.first);
                        copy[curr] = curr;
                        curr = next;
                        next = copy[curr];
                    }
                }

                entity_range = underlying_type::make_entity_range(pos_range);
            }
        }

        extendable_type* operator ->() ENTT_NOEXCEPT {
            return extendable;
        }
    protected:
        void instance_range_destroy(
            const pos_range_type& pos_range,
            const pos_type& erase,
            const pos_type& back,
            const pos_type& next,
            const pos_type& not_used) ENTT_NOEXCEPT
        {
            auto& instances = extendable->instances;
            const auto size = instances.size();

            auto* p = &instances[pos_range.first];

            for (auto i = size - 1; back <= i; --i, ++p) {
                *p = std::move(instances[i]);
            }

            instances.erase(instances.begin() + erase, instances.end());
        }
    protected:
        extendable_type* extendable = nullptr;
    };    
}